import React from 'react'
import { useState } from 'react'


const Calc = ({ onAdd } ) => {
    //  here i am setting the state of the inputs
    const [num, setNum] = useState(null)
    const [num2, setNum2] = useState(null)
    const [res, setResult] = useState(null)
    const [exp, setExp] = useState('')

    const onSubmit = (e) => {
        e.preventDefault()
   {/*  Here I check whether inputs are filled */}
        if(!num || !num2) {
            alert('Please enter a number')
    {/*  Here I check whether inputs have valid numbers*/}
        }else if(isNaN(num) || isNaN(num2)){
        alert('Please enter a valid Number')
        return
    }else{
        
   switch (exp){
       case "*":
            const result = num * num2
         setResult( result )
         break
         
        case "+":
        const result2 = parseInt(num) + parseInt(num2)
         setResult( result2 )
         break
         case "-":
            const result3 = parseInt(num)- parseInt(num2)
            setResult( result3 )
            break
        case "/":
            const result4 = num / num2
            setResult( result4 )
            
            break
        
        default:
    alert ('Choose operation')
    }
}
    }


    return (
        <div className="container">
            <h2>Calculator</h2>
       <form className='add-form' onSubmit={onSubmit}>
           <div className='form-control'>
               <label> Enter Number</label>
               <input type='num' placeholder='enter Number'
                value={num} onChange={(e)=> setNum(e.target.value)} />
           </div>
           <div className='form-control'>
               <label>Enter Number</label>
               <input type='num' placeholder='enter Number'
                 value={num2}
                 onChange={(e)=> setNum2(e.target.value)} />
           </div>

           {/* To add other operators at a later stage for now just Multipliy works */}
           <div className='form-control'>
               <div className='operators'>
               <button className='btn-multi'   onClick={()=> setExp("*")}>*</button>
               <button className='btn-multi'   onClick={()=> setExp("/")}>/</button>
               <button className='btn-multi'   onClick={()=> setExp("+")}>+</button>
               <button className='btn-multi'   onClick={()=> setExp("-")}>-</button>
               </div>
           </div>
           {/* <input type='submit' value='Calculate' className='btn btn-block' /> */}

       </form>

       {/*  Here I display the results */}
       <div className='Result'>
           {res}
       </div>

       </div>
    )
}


export default Calc

